package com.ayyappa.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.Serializable;

@SpringBootApplication
@EnableScheduling
public class RabbitmqApplication implements Serializable {

    public static final String EXCHANGE_NAME="amigos";
    public static final String DEFAULT_PARSING_QUEUE="chicasLocA";
    public static final String ROUTING_KEY="amigos_key";

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqApplication.class, args);
    }

    @Bean
    public TopicExchange amigosExchange(){
        return new TopicExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue amigosQueue(){
        return new Queue(DEFAULT_PARSING_QUEUE);
    }

    @Bean
    public Binding queueToExchangeBinding(){
        return BindingBuilder.bind(amigosQueue()).to(amigosExchange()).with(ROUTING_KEY);
    }
}
