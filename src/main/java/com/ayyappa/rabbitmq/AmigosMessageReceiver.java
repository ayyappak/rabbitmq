package com.ayyappa.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class AmigosMessageReceiver {
    private static final Logger log= LoggerFactory.getLogger(AmigosMessageSender.class);

    @RabbitListener(queues = RabbitmqApplication.DEFAULT_PARSING_QUEUE)
    public void consumeDefaultMessage(final Message message){
        log.info("---> Received Message"+message.toString());
    }
}
