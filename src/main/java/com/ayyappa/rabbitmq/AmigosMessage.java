package com.ayyappa.rabbitmq;

import java.io.Serializable;

public class AmigosMessage {
    private final String message;
    private final int priority;
    private final boolean secret;


    public AmigosMessage(String message, int priority, boolean secret) {
        this.message = message;
        this.priority = priority;
        this.secret = secret;
    }

    public String getMessage() {
        return message;
    }

    public int getPriority() {
        return priority;
    }

    public boolean isSecret() {
        return secret;
    }

    @Override
    public String toString() {
        return "AmigosMessage{" +
                "message='" + message + '\'' +
                ", priority=" + priority +
                ", secret=" + secret +
                '}';
    }
}
