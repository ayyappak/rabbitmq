package com.ayyappa.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;



@Service
public class AmigosMessageSender {

    private static final Logger log= LoggerFactory.getLogger(AmigosMessageSender.class);

    private final RabbitTemplate rabbitTemplate;

    public AmigosMessageSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 3000)
    public void sendAmigosMessage(){
        AmigosMessage mg=new AmigosMessage("hola freakin' amigos",1,false);
        rabbitTemplate.convertAndSend(RabbitmqApplication.EXCHANGE_NAME,RabbitmqApplication.ROUTING_KEY,mg.toString());

        log.info("hola message sent to dear amigos");
    }
}
